
Option Explicit

Dim aSharedFolders, aRemoveOptions

' list of folders that are shared to guests
aSharedFolders = Array("P:\", "Y:\", "Z:\")

' list of options to remove from source vmx
aRemoveOptions = Array("sharedFolder.*", _
                       "isolation\.tools\.hgfs\.disable.*", _
                       "tools\.remindInstall.*", _
                       "tools\.upgrade\.policy.*", _
                       "diskLib.sparseMaxFileSizeCheck.*", _
                       "uuid\..*", _
                       "ethernet\d+\.generatedAddress.*", _
                       "(scsi|ide)\d+:\d+\.mode.*")


' check arguments
Dim oArgs, sVMX, iCount
Set oArgs = WScript.Arguments
If oArgs.Length < 1 Then
  WScript.Echo "Usage: vmrun.vbs <vmxpath>"
  WScript.Quit
End If
For iCount=0 To oArgs.Count -1
  sVMX = sVMX & " " & oArgs(iCount)
Next
sVMX = Trim(sVMX)
Set oArgs = Nothing

' init base objects
Dim oFS, oShell, oNetwork
Set oFS = CreateObject("Scripting.FileSystemObject")
Set oShell = CreateObject("WScript.Shell")
Set oNetwork = WScript.CreateObject("WScript.Network")

' check that requested vmx file is found
If Not oFS.FileExists(sVMX) Then
  WScript.Echo "ERR: Path not found """ & sVMX & """."
  WScript.Quit
End If

' initialize regexps for removing options
Dim sRegexp
For iCount=0 To UBound(aRemoveOptions)
  sRegexp = aRemoveOptions(iCount)
  Set aRemoveOptions(iCount) = New RegExp
  aRemoveOptions(iCount).Pattern = "(" & sRegexp & ")"
  aRemoveOptions(iCount).IgnoreCase = True
Next

' get working directory and create it if it does not exist
Dim sVMFolder, sDestination
sVMFolder = oFS.BuildPath(oShell.ExpandEnvironmentStrings("%TEMP%"), "vmware")
If Not oFS.FolderExists(sVMFolder) Then
  oFS.CreateFolder(sVMFolder)
End If
sDestination = oFS.BuildPath(sVMFolder, _
                             oFS.GetFolder(oFS.GetFile(sVMX).ParentFolder).Name)
If Not oFS.FolderExists(sDestination) Then
  oFS.CreateFolder(sDestination)
End If


' copy vmx file
Dim oSource, oDestination, sLine, bSkip, oMatches
Set oSource = oFS.OpenTextFile(sVMX, 1)
Set oDestination = oFS.OpenTextFile(oFS.BuildPath(sDestination, oFS.GetFile(sVMX).Name), 2, True)
Do Until oSource.AtEndOfStream
  sLine = oSource.ReadLine
  bSkip = False
  For Each sRegexp In aRemoveOptions
    If sRegexp.Test(sLine) Then
      bSkip = True
      Exit For
    End If
  Next
  If Not bSkip Then
    Set sRegexp = New RegExp
    sRegexp.Pattern = "^((scsi|ide)\d+:\d+)\.fileName = ""?(.*\.vmdk)""?.*"
    Set oMatches = sRegexp.Execute(sLine)
    If oMatches.Count > 0 Then
      oDestination.WriteLine oMatches(0).SubMatches(0) & ".fileName = """ & _
          oFS.BuildPath(oFS.GetFile(sVMX).ParentFolder, _
          oMatches(0).SubMatches(2)) & """"
      oDestination.WriteLine oMatches(0).SubMatches(0) & _
          ".mode = ""independent-nonpersistent"""
    Else
      oDestination.WriteLine(sLine)
    End If
  End If
Loop

' add computer name running vmware into guestinfo.host option
oDestination.WriteLine("guestinfo.host = """ & oNetwork.ComputerName & """")

' add fix for running images via unc path
oDestination.WriteLine("diskLib.sparseMaxFileSizeCheck = ""false""")

' disable complaining of vmware tools
oDestination.WriteLine("tools.remindInstall = ""FALSE""")
oDestination.WriteLine("tools.upgrade.policy = ""Manual""")

' add shared folders
Dim sPath
iCount = 0
For Each sPath In aSharedFolders
  If oFS.FolderExists(sPath) Then
    oDestination.WriteLine("sharedFolder" & iCount & ".present = ""TRUE""")
    oDestination.WriteLine("sharedFolder" & iCount & ".enabled = ""TRUE""")
    oDestination.WriteLine("sharedFolder" & iCount & ".readAccess = ""TRUE""")
    oDestination.WriteLine("sharedFolder" & iCount & ".writeAccess = ""TRUE""")
    oDestination.WriteLine("sharedFolder" & iCount & ".hostPath = """ _
			   & sPath & """")
    oDestination.WriteLine("sharedFolder" & iCount & ".guestName = """ _
                           & Left(sPath, 1) & """")
    oDestination.WriteLine("sharedFolder" & iCount & ".expiration = ""session""")
    iCount = iCount + 1
  End If
Next
If iCount > 0 Then
  oDestination.WriteLine("sharedFolder.maxNum = """ & iCount & """")
  oDestination.WriteLine("isolation.tools.hgfs.disable = ""FALSE""")
End If

Set oDestination = Nothing
Set oSource = Nothing

oShell.Run """" & oFS.BuildPath(sDestination, oFS.GetFile(sVMX).Name) & """"
Set oFS = Nothing
Set oShell = Nothing
