
Option Explicit

Const ForReading = 1

Dim oFS, oNetwork, oShell
Set oFS = CreateObject("Scripting.FileSystemObject")
Set oNetwork = CreateObject("WScript.Network")
Set oShell = CreateObject("WScript.Shell")

Dim sDrive, sVmrunPath
sDrive = "\\st-vmware\vmware"
sVmrunPath = sDrive & "\bin\vmrun.vbs"

' Determine vmware icon path and quit if not found (eg. vmware not installed)
Dim sIconPath
On Error Resume Next
sIconPath = oShell.RegRead("HKCR\VMware.Document\DefaultIcon\")
If Err.Number Then
  WScript.Quit
End If
On Error Goto 0

' Init regexps
Dim oReConfig
Set oReConfig = New RegExp
oReConfig.IgnoreCase = True
oReConfig.Pattern = "^\s*([a-z\.0-9]*)\s*=\s*""?([^""]+)""?$"

' Get path to users start menu
Dim sShortcutFolder
sShortcutFolder = oShell.SpecialFolders("StartMenu") & _
                  "\Programs\VMware Images"

' Create shortcut directory if it doesn't exist
If Not oFS.FolderExists(sShortcutFolder) Then
  oFS.CreateFolder(sShortcutFolder)
End If

' Remove old shortcuts
Dim oFolder, oFile
Set oFolder = oFS.GetFolder(sShortcutFolder)
For Each oFile In oFolder.Files
  If Right(Lcase(oFile.Name), 4) = ".lnk" Then
    oFS.DeleteFile(oFile.Path)
  End If
Next

' Find image files and loop through them
Dim oImageFolder, iTest
On Error Resume Next
Set oImageFolder = oFS.GetFolder(sDrive & "\")
If Err.Number Then
  WScript.Quit
End If
On Error Goto 0
For Each oFolder In oImageFolder.SubFolders
  On Error Resume Next
  iTest = oFolder.Files.Count
  If Err.Number = 0 Then
    On Error Goto 0
    For Each oFile In oFolder.Files
      If Right(Lcase(oFile.Path), 4) = ".vmx" Then

        ' Get image information
        Dim sName, oFilePointer, sLine, oMatches
        sName = oFile.Name
        Set oFilePointer = oFS.OpenTextFile(oFile.Path, ForReading)
        Do Until oFilePointer.AtEndOfStream
          sLine = oFilePointer.ReadLine
          Set oMatches = oReConfig.Execute(sLine)
          If oMatches.Count = 1 Then
            If oMatches.Item(0).SubMatches.Count = 2 Then
              Dim sKey, sValue
              sKey = Lcase(oMatches.Item(0).SubMatches.Item(0))
              sValue = oMatches.Item(0).SubMatches.Item(1)
              If sKey = "displayname" Then
                sName = sValue
              End If
            End If
          End If
          Set oMatches = Nothing
        Loop
        Set oFilePointer = Nothing

        ' Create shortcut
        Dim oShortcut
        Set oShortcut = oShell.CreateShortcut( _
          sShortcutFolder & "\" & sName & ".lnk")
        oShortcut.TargetPath = sVmrunPath
        oShortcut.Arguments = oFile.Path
        oShortcut.WorkingDirectory = oShell.ExpandEnvironmentStrings("%TEMP%")
        oShortcut.IconLocation = sIconPath
        oShortcut.Save

      End If
    Next
  Else
    On Error Goto 0
  End If
Next

  ' Fix vmware preferences.ini
Dim sPreferences
sPreferences = oFS.BuildPath(oShell.ExpandEnvironmentStrings("%APPDATA%"), _
                             "VMware\preferences.ini")
If Not oFS.FileExists(sPreferences) Then
  If Not oFS.FolderExists(oFS.GetParentFolderName(sPreferences)) Then
    oFS.CreateFolder(oFS.GetParentFolderName(sPreferences))
  End If
  Set oFile = oFS.CreateTextFile(sPreferences)
  oFile.WriteLine(".encoding = ""windows-1252""")
  oFile.WriteLine("pref.vmplayer.exit.vmaction = ""poweroff""")
  oFile.WriteLine("pref.vmplayer.exit.softly = FALSE")
  oFile.WriteLine("pref.enableAllSharedFolders = TRUE")
  oFile.WriteLine("hint.lsilogic.needDriver = FALSE")
  oFile.WriteLine("hint.keyboardHook.hookTimeout = FALSE")
  oFile.WriteLine("pref.keyboardAndMouse.maxProfiles = 0")
  oFile.WriteLine("pref.vmplayer.dataCollectionEnabled = FALSE")
  oFile.WriteLine("pref.autoSoftwareUpdatePermission = ""deny""")
  oFile.WriteLine("pref.componentDownloadPermission = ""deny""")
  oFile.Close
  Set oFile = Nothing
End If

Set oShell = Nothing
Set oFS = Nothing
Set oNetwork = Nothing
